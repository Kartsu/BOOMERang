﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PostProcessing : MonoBehaviour
{
    UnityEngine.Rendering.VolumeProfile volumeProfile;
    UnityEngine.Rendering.Universal.LensDistortion lensDistortion;
    UnityEngine.Rendering.Universal.Bloom bloom;
    UnityEngine.Rendering.Universal.Vignette vignette;
    UnityEngine.Rendering.Universal.ColorAdjustments colorAdjustment;

    [Header("LensDistortion Settings")]
    [SerializeField] float maxLensIntensity = .45f;
    [SerializeField] float lensGrowDuration = .1f;
    [SerializeField] float lensShrinkDuration = .2f;

    [Header("Bloom Settings")]
    [SerializeField] float maxBloomIntensity = 8f;
    [SerializeField] float minBloomIntensity = 4f;
    [SerializeField] float bloomGrowDuration = .1f;
    [SerializeField] float bloomShrinkDuration = .2f;

    [Header("Vignette Settings")]
    [SerializeField] float maxVignetteIntensity = 0.5f;
    [SerializeField] float minVignetteIntensity = 0f;
    [SerializeField] float vignetteGrowDuration = .1f;
    [SerializeField] float vignetteShrinkDuration = .4f;

    [Header("Post Exposure Settings")]
    [SerializeField] float maxPostExposureIntensity = 0.5f;
    [SerializeField] float minPostExposureIntensity = 0f;
    [SerializeField] float PostExposureGrowDuration = .1f;
    [SerializeField] float PostExposureShrinkDuration = .4f;

    Tween vignetteTween;
    Tween lensTween;
    float lensIntensity = 0;
    float bloomIntensity = 0;
    float vignetteIntensity = 0;
    float postExposureIntensity = 0;
    Tween bloomTween;
    Tween postExposureTween;

    // Start is called before the first frame update
    void Start()
    {
        volumeProfile = GetComponent<UnityEngine.Rendering.Volume>()?.profile;
        if (!volumeProfile) throw new System.NullReferenceException(nameof(UnityEngine.Rendering.VolumeProfile));

        // You can leave this variable out of your function, so you can reuse it throughout your class.

        if (!volumeProfile.TryGet(out lensDistortion)) throw new System.NullReferenceException(nameof(lensDistortion));

        if (!volumeProfile.TryGet(out bloom)) throw new System.NullReferenceException(nameof(bloom));

        if (!volumeProfile.TryGet(out vignette)) throw new System.NullReferenceException(nameof(vignette));

        if (!volumeProfile.TryGet(out colorAdjustment)) throw new System.NullReferenceException(nameof(colorAdjustment));

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DistortLens()
    {

    }

    public void AnimateLensDistortion(bool value)
    {
        lensTween = DOTween.To(() => lensIntensity,
            x => lensIntensity = x, value ? maxLensIntensity : 0, lensGrowDuration).OnUpdate(UpdateLensDistortionAnimation);
        lensTween.OnComplete(OnLensDistortionAnimationEnd);

        bloomTween = DOTween.To(() => bloomIntensity,
            x => bloomIntensity = x, value ? maxBloomIntensity : 0, bloomGrowDuration).OnUpdate(UpdateBloomAnimation);
        bloomTween.OnComplete(OnBloomAnimationEnd);
    }

    public void AnimatePostExposure(float value)
    {
        /*
        postExposureTween = DOTween.To(() => postExposureIntensity,
            x => postExposureIntensity = x, value ? maxPostExposureIntensity: -2, PostExposureGrowDuration).OnUpdate(UpdatePostExposureAnimation);
        postExposureTween.OnComplete(OnPostExposureEnd);
        */
        postExposureTween = DOTween.To(() => postExposureIntensity,
            x => postExposureIntensity = x, value, PostExposureGrowDuration).OnUpdate(UpdatePostExposureAnimation);
        postExposureTween.OnComplete(OnPostExposureEnd);
    }

    public void AnimateDamaged(bool value)
    {
        if (vignette.intensity == minVignetteIntensity)
        {
            lensTween = DOTween.To(() => vignetteIntensity,
        x => vignetteIntensity = x, value ? maxVignetteIntensity : 0, vignetteGrowDuration).OnUpdate(UpdateVignetteAnimation);
            lensTween.OnComplete(OnVignetteAnimationEnd);
        }
    }

    void UpdateLensDistortionAnimation()
    {
        lensDistortion.intensity.value = lensIntensity;
        //profile.LensDistortion.settings = vign;
    }
    void UpdateBloomAnimation()
    {
        bloom.intensity.value = bloomIntensity;
        //profile.LensDistortion.settings = vign;
    }

    void UpdatePostExposureAnimation()
    {
        colorAdjustment.postExposure.value = postExposureIntensity;
        //profile.LensDistortion.settings = vign;
    }

    void UpdateVignetteAnimation()
    {
        vignette.intensity.value = vignetteIntensity;
        //profile.LensDistortion.settings = vign;
    }


    void OnLensDistortionAnimationEnd()
    {
        lensTween = null;
        AnimateBack(false);
    }

    void OnBloomAnimationEnd()
    {
        bloomTween = null;
        BloomAnimateBack(true);
    }

    void OnPostExposureEnd()
    {
        bloomTween = null;
        BloomAnimateBack(true);
    }

    void OnVignetteAnimationEnd()
    {
        vignetteTween = null;
    }

    void AnimateBack(bool value)
    {
        lensTween = DOTween.To(() => lensIntensity,
    x => lensIntensity = x, value ? 0 : 0, lensShrinkDuration).OnUpdate(UpdateLensDistortionAnimation);
    }

    void BloomAnimateBack(bool value)
    {
        bloomTween = DOTween.To(() => bloomIntensity,
    x => bloomIntensity = x, value ? minBloomIntensity : 0, bloomShrinkDuration).OnUpdate(UpdateBloomAnimation);
    }

    public void VignetteAnimateBack(bool value)
    {
        vignetteTween = DOTween.To(() => vignetteIntensity,
    x => vignetteIntensity = x, value ? minVignetteIntensity : 0, vignetteShrinkDuration).OnUpdate(UpdateVignetteAnimation);
    }

    /*

    public void SetReversePostProcessing()
    {
        colorAdjustments.colorFilter.Override(new Color(172f / 255f, 255f / 255f, 196f / 255f));
    }


    public void SetNormalPostProcessing()
    {
        colorAdjustments.colorFilter.Override(new Color(255f / 255f, 255f / 255f, 255f / 255f));
    }

    public void SetUnstoppablePostProcessing()
    {
        colorAdjustments.colorFilter.Override(new Color(206f / 255f, 234f / 255f, 255f / 255f));
    }
    */
}
