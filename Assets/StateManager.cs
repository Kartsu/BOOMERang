using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StateManager : MonoBehaviour
{
    AudioSource audio;
    DoTweenSpeed tween;
    public GameObject player;
    public AudioClip sog;
    public bool isPlaying = false;
    public int score = 0;
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        tween = player.GetComponent<DoTweenSpeed>();
        isPlaying = false;
        score = 0;
    }

    public void increaseScore()
    {
        score++;
    }

    public int getScore()
    {
        return score;
    }


    public void play()
    {
        audio.PlayOneShot(sog);
        tween.startPath();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!isPlaying)
            {
                play();
                isPlaying = true;
            }
        }
    }
}
