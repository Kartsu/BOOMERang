using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

public class DoTweenSpeed : MonoBehaviour
{
    public GameObject circle;
    public GameObject note;
    public GameObject stateManager;
    public GameObject zhongli;
    public GameObject amber;
    public GameObject club;
    private int counter;
    private Tween dotweenPath;
    private Vector2 zeroQuarters;
    private Vector2 oneQuarters;
    private Vector2 twoQuarters;
    private Vector2 threeQuarters;
    private Vector2 fourQuarters;
    [SerializeField]
        UnityEvent m_start = default;
        public event UnityAction start
    {
        add { m_start.AddListener(value); }
        remove { m_start.RemoveListener(value); }
    }
    [SerializeField]
    UnityEvent m_start2 = default;
    public event UnityAction start2
    {
        add { m_start2.AddListener(value); }
        remove { m_start2.RemoveListener(value); }
    }
    [SerializeField]
    UnityEvent m_Intro2 = default;
    public event UnityAction Intro2
    {
        add { m_Intro2.AddListener(value); }
        remove { m_Intro2.RemoveListener(value); }
    }
    [SerializeField]
    UnityEvent m_introEnd = default;
    public event UnityAction introAnd
    {
        add { m_introEnd.AddListener(value); }
        remove { m_introEnd.RemoveListener(value); }
    }
    [SerializeField]
    UnityEvent m_bridgeStart = default;
    public event UnityAction bridgeStart
    {
        add { m_bridgeStart.AddListener(value); }
        remove { m_bridgeStart.RemoveListener(value); }
    }
    [SerializeField]
    UnityEvent m_bridgeEnd = default;
    public event UnityAction bridgeEnd
    {
        add { m_bridgeEnd.AddListener(value); }
        remove { m_bridgeEnd.RemoveListener(value); }
    }
    [SerializeField]
    UnityEvent m_chorusStart = default;
    public event UnityAction chorusStart
    {
        add { m_chorusStart.AddListener(value); }
        remove { m_chorusStart.RemoveListener(value); }
    }
    [SerializeField]
    UnityEvent m_end = default;
    public event UnityAction end
    {
        add { m_end.AddListener(value); }
        remove { m_end.RemoveListener(value); }
    }
    void Onend()
    {
        m_end.Invoke();
    }
    void OnchorusStart()
    {
        m_chorusStart.Invoke();
    }
    void OnbridgeEnd()
    {
        m_bridgeEnd.Invoke();
    }
    void OnbridgeStart()
    {
        m_bridgeStart.Invoke();
    }


    void OnintroEnd()
    {
        m_introEnd.Invoke();
    }
    void OnIntro2()
    {
        m_Intro2.Invoke();
    }
    void Onstart2()
    {
        m_start2.Invoke();
    }
    [SerializeField]
    UnityEvent m_threeStar = default;
    public event UnityAction threeStar
    {
        add { m_threeStar.AddListener(value); }
        remove { m_threeStar.RemoveListener(value); }
    }
    void OnthreeStar()
    {
        m_threeStar.Invoke();
    }

    [SerializeField]
    UnityEvent m_fourStar = default;
    public event UnityAction fourStar
    {
        add { m_fourStar.AddListener(value); }
        remove { m_fourStar.RemoveListener(value); }
    }
    void OnfourStar()
    {
        m_fourStar.Invoke();
    }

    [SerializeField]
    UnityEvent m_fiveStar = default;
    public event UnityAction fiveStar
    {
        add { m_fiveStar.AddListener(value); }
        remove { m_fiveStar.RemoveListener(value); }
    }
    void OnfiveStar()
    {
        m_fiveStar.Invoke();
    }

    void Onstart()
    {
        m_start.Invoke();
    }
    // Start is called before the first frame update
    void Start()
    {
        counter = 0;
        dotweenPath = circle.GetComponent<DOTweenPath>().GetTween();
        dotweenPath.Pause();
        zeroQuarters = new Vector2(-5.915113f, 0);
        oneQuarters = new Vector2(-3.535165f, 2.915906f);
        twoQuarters = new Vector2(0.02244067f, 4.000588f);
        threeQuarters = new Vector2(3.58f, 2.91f);
        fourQuarters = new Vector2(5.95f, 0);

    }

    public void startPath()
    {
        dotweenPath.Play();
    }

    public void countUp()
    {
        counter++;
        // adjusting the speed of the player
        if (counter == 29)
        {
 
            dotweenPath.timeScale = 2;
        }
        else if (counter == 43)
        {
            dotweenPath.timeScale = 1;
        }
        else if (counter == 52)
        {
            dotweenPath.timeScale = 2;
        }
        else if (counter == 55)
        {
            roll(stateManager.GetComponent<StateManager>().score);
        }

        

        // spawning the notes
        if ((counter >= 4) && (counter < 21)) {
            if (counter % 2 == 1) {
                genNote(zeroQuarters);
                genNote(oneQuarters);
            }
            else if (counter % 2 == 0)
            {
                genNote(fourQuarters);
            }
            if ((counter == 12) || (counter == 20))
            {
                genNote(twoQuarters);
            }
        }

        if ((counter > 27) && (counter < 43))
        {
            if (counter % 2 == 1)
            {
                genNote(zeroQuarters);
            } else
            {
                genNote(fourQuarters);
            }
            if (counter == 36)
            {
                genNote(twoQuarters);
            }
            if (counter > 35)
            {
                genNote(twoQuarters);
            }
        }

        if ((counter > 42) && (counter < 52))
        {
            if (counter % 2 == 1)
            {
                genNote(zeroQuarters);
            }
            else if (counter % 2 == 0)
            {
                genNote(fourQuarters);
            }
            if (counter == 44)
            {
                genNote(threeQuarters);
            }
            if (counter == 45)
            {
                genNote(twoQuarters);
            }
            if (counter == 46)
            {
                genNote(threeQuarters);
            }
            if (counter == 47)
            {
                genNote(twoQuarters);
                genNote(oneQuarters);
            }
            if (counter == 48)
            {
                genNote(threeQuarters);
            }
            if (counter == 49)
            {
                genNote(twoQuarters);
            }
            if (counter == 50)
            {
                genNote(threeQuarters);
            }
            if (counter == 51)
            {
                genNote(twoQuarters);
                genNote(oneQuarters);
            }
        }
        if (counter == 52)
        {
            genNote(fourQuarters);
        }


        // Unity Events
        if (counter == 5)
        {
            Onstart2();
        } else if (counter == 12)
        {
            OnIntro2();
        } else if (counter == 21)
        {
            OnintroEnd();
        } else if (counter == 27)
        {
            OnbridgeStart();
        } else if (counter == 43)
        {
            OnbridgeEnd();
        } else if (counter == 44)
        {
            OnchorusStart();
        } else if (counter == 53)
        {
            Onend();
        }
    }

    private void roll(int score)
    {
        print(score);
        if (score == 70)
        {
            VideoPlayer video = zhongli.GetComponent<UnityEngine.Video.VideoPlayer>();
            video.Prepare();
            video.Play();
        }
        else if (score < 64)
        {
            VideoPlayer video = club.GetComponent<UnityEngine.Video.VideoPlayer>();
            video.Prepare();
            video.Play();
        }
        else
        {
            VideoPlayer video = amber.GetComponent<UnityEngine.Video.VideoPlayer>();
            video.Prepare();
            video.Play();
        }
    }

    private void genNote(Vector2 position)
    {
        Instantiate(note, position, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
