using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    [SerializeField]
    UnityEvent m_noteBreak = default;
    public event UnityAction noteBreak
    {
        add { m_noteBreak.AddListener(value); }
        remove { m_noteBreak.RemoveListener(value); }
    }
    void OnnoteBreak()
    {
        m_noteBreak.Invoke();
    }
    List<GameObject> notes;
    public StateManager stateManager;

    // Start is called before the first frame update
    void Start()
    {
        notes = new List<GameObject>();
    }



    // Update is called once per frame
    void Update()
    {
        if (stateManager.isPlaying) {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (notes.Count > 0)
                {
                    stateManager.increaseScore();
                    GameObject note = notes[0];
                    this.deleteNote(note);
                    OnnoteBreak();
                }
            }
            transform.Rotate(new Vector3(0, 0, 30) * Time.deltaTime * 5);
        }
    }
   
    public void addNote(GameObject note)
    {
        if (!notes.Contains(note))
        {
            notes.Add(note);
        }
    }

    public void deleteNote(GameObject note)
    {
        if (notes.Contains(note))
        {
            notes.Remove(note);
            Destroy(note);
        }
    }
}
