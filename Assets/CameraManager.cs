using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraManager : MonoBehaviour
{

    CinemachineVirtualCamera _cam;
    CinemachineImpulseSource _impulseSource;
    // Start is called before the first frame update
    void Start()
    {
        _cam = GetComponent<CinemachineVirtualCamera>();
        _impulseSource = GetComponent<CinemachineImpulseSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ZoomOutLens()
    {
        _cam.m_Lens.OrthographicSize = 5.0f;
        _impulseSource.m_ImpulseDefinition.m_AmplitudeGain = 10.0f;
    }

    public void ZoomInLens()
    {
        _cam.m_Lens.OrthographicSize = 1.55f;
        _impulseSource.m_ImpulseDefinition.m_AmplitudeGain = 1.0f;
    }
}
